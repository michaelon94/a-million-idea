<?php

session_start();
include('connection.php');
require_once('registration.php');

$regObj =  new Registration();

if(isset ($_POST['register'])){
    $errors = array();

 $required_fields = array('name','email','password', 'cpassword');
         foreach ($required_fields as $k => $fieldname) {
             if (empty($_POST[$fieldname])) {
                 $errors[$k] = $fieldname. ' field is required';
             }
         }
         if(!empty($_POST['cpassword']) && ($_POST['password'] != $_POST['cpassword'])){
         	$errors[] = 'password field an confirm password field do not match';
         }

		if (empty($errors)) {
            $name = $_POST['name'];
         $email = $_POST['email'];
         $password = $_POST['password'];

         $reg = $regObj->register($name, $email, $password);
              
             
            if ($reg == true) {
                         
                redirect("index.php");
                        
           }else{
                
                $_SESSION['msg'] = $reg;
                redirect("login.php");
               }

          }else{
              //$_SESSION['error_msg'] = "There were" .(count($errors) == 1) ? count($errors). "error in the form" :  count($errors). "errors in the form";
          	  $_SESSION['errors'] = $errors;
          	  redirect("login.php");
          }
}
        
     
    

if(isset ($_POST['login']))
{

	    if (empty($error1)) {
	    	
	    $email = $_POST['email'];
	    $password = $_POST['password'];
      
	    $reg1 = $regObj->login($email, $password);
    
        if($reg1 == true){
	    redirect('index.php');
           }
            else{
	 	$_SESSION['msg1'] = $reg1;
	   redirect('login.php');
	    }
	  }
	    //else{
	    //	$_SESSION['error1'] = $error1;
	    //	redirect('login.php');
	    //}
}
	function redirect($url)
	{
	  header('location:'.$url);
	}

if(isset($_POST['submit']))
{
  $err = "";
  if (empty($_POST['email'])) {
  	$err = "The field cannot be empty";
  }
    if (empty($err)) {
    	$email = $_POST['email'];

    	$let  = $regObj->newsletter($email);
    	if ($let == true) {
    		redirect('confirm.php');
    	}
    	else
    	{
    		$_SESSION['err'] = $let;
    		redirect('join-the-initiative.php');
    	}
    }else{
    	$_SESSION['err'] = $err;
    	redirect('join-the-initiative.php');
    }
}
?>