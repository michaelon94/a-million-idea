<!DOCTYPE html>
    <html lang="en">
      <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="../../favicon.ico">

        <title>  A million ideas </title>

          <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="fonts/font-awesome/css/font-awesome.min.css">

        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        
        <link href="css/custom.css" rel="stylesheet">
         

        <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
        <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
        <script src="../../assets/js/ie-emulation-modes-warning.js"></script>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
      </head>
<body>
 <div class = "header_top">
  <div class="container">
    <div class= "row">
      <div class="col-sm-6">
         <div class="contactinfo">
           <ul class="nav nav-pills">
            <li><a href="#">
                <i class="fa fa-phone"></i>+234 810 566 7449</a>
          </li>
          <li>
            <a href="#">
              <i class="fa fa-envelope"></i>
            michaelonyeforo112@gmail.com</a>
          </li>
          </ul>
         </div>
      </div>
      <div class = "col-sm-6">
        <div class="social-icons pull-right">
         <ul class="nav navbar-nav">
          <li><a href="#"><i class="fa fa-facebook"></i></a></li>
          <li><a href="#"><i class="fa fa-twitter"></i></a></li>
          <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
          <li><a href="#"><i class="fa fa-google-plus"></i></a></li>

        </ul>
       </div>
      </div>
    </div>
  </div>
</div>
  
  
</body>