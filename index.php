<?php session_start()?>
<!DOCTYPE html>
    <html lang="en">
      <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="../../favicon.ico">

        <title>  A million ideas </title>

          <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="fonts/font-awesome/css/font-awesome.min.css">

        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="css/custom.css" rel="stylesheet">
         

        <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
        <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
        <script src="../../assets/js/ie-emulation-modes-warning.js"></script>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
      </head>

      <body>

        <nav class="navbar navbar-inverse navbar-fixed-top">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <!--<a class="navbar-brand" href="#"><img src="images/idea.jpg"></a>-->
            </div>
            <div id="navbar" class="collapse navbar-collapse">
              <ul class="nav navbar-nav navbar-right">
              <li class="active"><a href="index.php"><i class="fa fa-home" ></i> HOME</a></li>
               <?php if(isset($_SESSION['id'])):?>
                  <li><a href="user.php">Welcome, <?php echo $_SESSION['email']?> </a></li>
                  <li><a href="logout.php">LOG OUT</a></li>
              <?php else :?>
                  <li><a href="login.php"><i class="fa fa-user  "></i>  REGISTER / SIGN IN</a></li>
              <?php endif?>
                  <li><a href="#who"><i class="fa fa-building-o   "> </i>  WHO WE ARE</a></li>
                <li class="dropdowm"><a href="#">GET INVOLVED<i class="fa fa-angle-down  ">  </i> </a>
                <ul role="menu" class="sub-menu">
                <li><a href="#">Start Learning</a></li>
                </ul>
                </li>

              </ul>
            </div><!--/.nav-collapse -->
          </div>
        </nav>

        <!-- Main jumbotron for a primary marketing message or call to action -->
        <div class="jumbotron">
          <div class="container">
              <h1>Our goal is to educate and inspire <br>Outstanding ideas</br> that will change the   world_</h1>
            <p>we know that there are strong ideas that will rule the world, but individual with these ideas tend to be afraid to  let light shine on their ideas, 
            but we've come to know that no idea is stupid nor strange, an idea is like a seed, 
            until you plant it, it won't germinate. <strong><em>Ideas change the world.</em></strong></p>
    <p><a class="btn btn-danger btn-lg" href="#" role="button">Learn more &raquo;</a></p>
            <!--<img src="images/code_logo.png" title="code.org logo" alt="code.org">-->
          </div>
        </div>

        <div class="container homepage">
             <span class="fa-stack fa-4x">
                <i class="fa fa-circle fa-stack-2x"></i>
                    <i class="fa fa-users fa-stack-1x fa-inverse"></i>
                   </span>
            <div id="who" class="container homepage"></div>
        <h2>Who Are We</h2>
            <hr>
            <p>We are projectONE, our aim is to help young individuals with awesome ideas bring their dream  to life, help entrepreneurs manage their project by giving the necessary guidelines in the system to keep them moving forward. In a sentence, we help you achieve your goal.
            <hr>
          <!-- Example row of columns -->
          <div class="row">
            <div class="col-md-4">
                <span class="fa-stack fa-4x">
                <i class="fa fa-circle fa-stack-2x"></i>
                    <i class="fa fa-user fa-stack-1x fa-inverse"></i>
                   </span>
              <h3>you</h3>
              <p>Have an idea but think it's stupid or strange? Are you looking for ways to make this idea generate income  for you? Click he button below and we will show you how to get started.</p>
              <p><a class="btn btn-default" href="#" role="button">Start learning &raquo;</a></p>
            </div>
            <div class="col-md-4">
                <span class="fa-stack fa-4x">
                <i class="fa fa-circle fa-stack-2x"></i>
                    <i class="fa fa-pencil fa-stack-1x fa-inverse"></i>
                   </span>
              <h3>enterpreneurs</h3>
              <p>Have an idea that will you bring your company to the next level, maybe get you promoted? Awesome! Click the button below and we will show you how. </p>
              <p><a class="btn btn-default" href="join-the-initiative.php" role="button">Join the initiative &raquo;</a></p>
           </div>
            <div class="col-md-4">
                <span class="fa-stack fa-4x">
                <i class="fa fa-circle fa-stack-2x"></i>
                    <i class="fa fa-money fa-stack-1x fa-inverse"></i>
                   </span>
              <h3>sponsors</h3>
              <p>We love all the support we get to help host more events, and empower more lives with the knowledge of how to make their dream come through, Click to get involved.</p>
              <p><a class="btn btn-default" href="#" role="button">Give support &raquo;</a></p>
            </div>
          </div>
    </div> <!-- /container -->

        <div class="footer-bottom">
  <div class="container">
  <div class="row">
  <div class="col-sm-9">
    <p>copyright @ 2015 | Onyeforo | All rights reserved.</p>
    </div>
    <div class="soc">
    <p class="pull-right"><span><a target="_blank" href="twitter.com/michael"><i class="fa fa-twitter"></i></a></span></p>
    <p class="pull-right"><span><a href="plus.google.com/michael"><i class="fa fa-google-plus"></i></a></span></p>
    <p class="pull-right"><span><a href="facebook.com/michael"><i class="fa fa-facebook"></i></a></span></p>
    <p class="pull-right"><span><a href="pinterest.com/michael"><i class="fa fa-pinterest"></i></a></span></p>
        <p class="pull-right"><span>Connect with us </span></p>


  </div>
  </div>
</div>
</div>
        
     
          
        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
      </body>
    </html>
