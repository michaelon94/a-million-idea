<!DOCTYPE html>
<?php 
   session_start(); session_destroy();
   ?>
    <html lang="en">
      <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="../../favicon.ico">

        <title>  A million ideas </title>

          <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="fonts/font-awesome/css/font-awesome.min.css">

        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        
        <link href="css/custom.css" rel="stylesheet">
         

        <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
        <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
        <script src="../../assets/js/ie-emulation-modes-warning.js"></script>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
      </head>

<body>
    <nav class="navbar navbar-inverse navbar-fixed-top">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              </div>
            <div id="navbar" class="collapse navbar-collapse">
               <ul class="nav navbar-nav navbar-right">
              <li><a href="index.php"><i class="fa fa-home" ></i> HOME</a></li>
               <?php if(isset($_SESSION['id'])):?>
                  <li><a href="user.php">Welcome, <?php echo $_SESSION['email']?> </a></li>
                  <li><a href="logout.php">LOG OUT</a></li>
              <?php else :?>
                  <li><a href="login.php"><i class="fa fa-user  "></i>  REGISTER / SIGN IN</a></li>
              <?php endif?>
                  <li><a href="#who"><i class="fa fa-building-o   "> </i>  WHO WE ARE</a></li>
                <li><a href="join-the-initiative.php"><i class="fa fa-sign-in    ">  </i>  GET INVOLVED</a></li>

              </ul>
            </div><!--/.nav-collapse -->
          </div>
        </nav>

<div class="jumbotron">
          <div class="container">
              <h1>Our goal is to educate and inspire <br>Outstanding ideas</br> that will change the   world_</h1>
          </div>
        </div>

   <div class="container join">
     <h2> JOIN THE INITIATIVE</h2>
    <div class="row">
            <div class="col-md-3">
                <div class="stack">
             <span class="fa-stack fa-4x">
                <i class="fa fa-circle fa-stack-2x"></i>
                    <i class="fa fa-pencil fa-stack-1x fa-inverse"></i>
                </span>
            </div>
        </div>
            <div class = "col-md-9">
                <div class="page">
                <p>As an entrepreneur, you know and understand how helpful ideas are to you and your business, probably you have this million idea but doesn't know how to go about? we are here to  help. </p>
            </div>
        </div>
        </div>
    </div>
<div class = "container entrepreneur"></div>
<footer id="footer">
<div class="footer-top">
<div class="container">
  <div class="row">
    <div class="col-md-6">
       <div class="info">
         <h2>A million idea</h2>
         <p>Idea, Idea , Idea will take you to places you've never imagined, your goals will start will an idea, you don't have to give up any idea 
         that comes in your head, it could worth millions!</p>
       </div>
    </div>
    <div class="col-md-3">
      <div class="org">
        <h2>our organization</h2>
        <ul class="org-info" style="padding:10px 10px 10px; font-weight: 300; border: 1px solid #F7F7F0;">
          <li><a href="">About projectOne</a></li>
           <li><a href="">Services</a></li>
            <li><a href="">Careers</a></li>
             <li><a href="">Latest News</a></li>
        </ul>
      </div>
    </div>
    <div class="col-md-3">
      <div class="news-letter">
        <h2>News Letter</h2>
        <form action="action.php" method="POST">
        <?php if (isset($_SESSION['err']))  
        {
           echo "<p class='alert alert-danger' style = 'text-transform: lowercase; font-size:15px'>";
          echo "please review your email: <br />". "field cannot be empty";
          echo "</p>";
        }?>
          <input placeholder="Email Address" type="email" name="email">
          <a href="#" data-toggle="modal" data-target="#newsletter"><button class="btn btn-danger" >Get the latest news</button></a>
          <label><input type="hidden" name="submit"></label>
        </form>
      </div>
    </div>
  </div>
</div>
</div>
<div class="modal" id="newsletter" tabindex="-1" role="dialog"> 
   <div class="modal-dialog">
     <div class="modal-content">
       <div class="modal-header">
       <h2>Thank you message</h2>
       </div>
       <div class="modal-body">
         <div class="panel panel-default">
               <div class="panel-heading"></div>
               <div class="panel-body"><p>Thank you for your interest in a million idea, and also for subscribing to our newsletter. We will send you the latest news, events and programmes as regard to how you are to go about your idea(s).</p></div>
               
            </div>
       </div>
       <div class="modal-footer">
         <div class="p-btn">
                   <a href="index.php"><button class="btn btn-danger">Close</button></a>
               </div>
       </div>
     </div>
   </div>
</div>
<div class="footer-bottom">
  <div class="container">
  <div class="row">
  <div class="col-sm-9">
    <p>copyright @ 2015 | Onyeforo | All rights reserved.</p>
    </div>
    <div class="soc">
    <p class="pull-right"><span><a target="_blank" href="twitter.com/michael"><i class="fa fa-twitter"></i></a></span></p>
    <p class="pull-right"><span><a href="plus.google.com/michael"><i class="fa fa-google-plus"></i></a></span></p>
    <p class="pull-right"><span><a href="facebook.com/michael"><i class="fa fa-facebook"></i></a></span></p>
    <p class="pull-right"><span><a href="pinterest.com/michael"><i class="fa fa-pinterest"></i></a></span></p>
        <p class="pull-right"><span>Connect with us </span></p>


  </div>
  </div>
</div>
</div>

</footer>
</body>
</html>