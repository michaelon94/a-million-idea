<!DOCTYPE html>
<?php
session_start(); session_destroy();
include ('connection.php');
require_once ('registration.php');
    ?>
<head>
    <html lang="en">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="../../favicon.ico">
             <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="fonts/font-awesome/css/font-awesome.min.css">

        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="css/custom.css" rel="stylesheet">

        <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
        <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
        <script src="../../assets/js/ie-emulation-modes-warning.js"></script>

</head>

   <body>
     <nav class="navbar navbar-inverse navbar-fixed-top">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
                <!--<a class="navbar-brand" href="#"><img src="images/one_million_top_logo.png"></a>-->
            </div>
            <div id="navbar" class="collapse navbar-collapse">
              <ul class="nav navbar-nav navbar-right">
                  <li><a href="index.php">HOME</a></li>
                  <!--<li><a href="login.php">REGISTER / SIGN IN</a></li>-->
                  <li><a href="#who">ABOUT US</a></li>
                <li><a href="join-the-initiative.php">GET INVOLVED</a></li>

              </ul>
            </div><!--/.nav-collapse -->
          </div>
        </nav>
      
<section id="form">
        <div class="container">
            <p>Account Details</p>
            <div class="container">
            <div class="row">
            <div class="col-sm-4 col-sm-offset-1">
                <div class="login-form">
                    <h2>Login to your account</h2>
                    <form action="action.php" method="post">
                     <?php if (isset($_SESSION['msg1'])) {
                                 echo "<p class='alert alert-danger' style = 'text-transform: lowercase; font-size:15px'>";
                                echo $_SESSION['msg1'] ."username/password does not exist";
                                 echo "</p>";
                            }?>
                        <input type="text" placeholder="Email Address" name="email" />
                        <input type="password" placeholder="Password" name="password" />
                <span>
                    <input class="checkbox" type="checkbox" ></input>
                  Keep me signed in
                </span>
                <button class="btn btn-default" type="submit">Login</button>
                <label>
				<input type="hidden" name="login">
				</label>
                </form>
                </div>
                </div>
                <div class="col-sm-1"><h2 class="or">OR</h2>
        </div>
                <div class="col-sm-4">
        <div class="signup-form"><!--sign up form-->
						<h2>New User Signup!</h2>
						<form action="action.php" method="POST">
                            
                            <?php if (isset($_SESSION['errors'])) {
                                echo "<p class='alert alert-danger' style = 'text-transform: lowercase; font-size:15px'>";
                                echo "Please review the following fields:";
                                echo "<br />";
                                foreach ($_SESSION['errors'] as $error) {
                                    echo $error. "<br />";
                                }
                                echo "</p>";
                            }?>
                            <?php if (isset($_SESSION['msg'])) {
                                 echo "<p class='alert alert-danger' style = 'text-transform: lowercase; font-size:15px'>";
                                echo $_SESSION['msg'];
                                 echo "</p>";
                            }?>
							<input placeholder="Name" type="text" name="name">
							<input  placeholder="Email Address" type="email" name="email">
							<input  placeholder="Password" type="password" name="password">
                            <input  placeholder="Confirm Password" type="password" name="cpassword">
							<button type="submit" class="btn btn-default">Signup</button>
							<label>
							<input type="hidden" name="register"</label>
						</form>
					</div>
                    </div>
            </div>
            </div>
            </div>   
        </section>
</body>
 <div class="footer-bottom">
  <div class="container">
  <div class="row">
  <div class="col-sm-9">
    <p>copyright @ 2015 | Onyeforo | All rights reserved.</p>
    </div>
    <div class="soc">
    <p class="pull-right"><span><a target="_blank" href="twitter.com/michael"><i class="fa fa-twitter"></i></a></span></p>
    <p class="pull-right"><span><a href="plus.google.com/michael"><i class="fa fa-google-plus"></i></a></span></p>
    <p class="pull-right"><span><a href="facebook.com/michael"><i class="fa fa-facebook"></i></a></span></p>
    <p class="pull-right"><span><a href="pinterest.com/michael"><i class="fa fa-pinterest"></i></a></span></p>
        <p class="pull-right"><span>Connect with us </span></p>


  </div>
  </div>
</div>
</div>
</html>